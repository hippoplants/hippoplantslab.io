![Build Status](https://gitlab.com/hippoplants/hippoplants.gitlab.io/badges/master/build.svg)

---

# Source Code for [hippoeatsplants] Website

[hippoeatsplants]: https://hippoplants.gitlab.io

---

## Development

The canonical documentation for the intersection of tech this website uses
(which is Gitlab CI/CD, Gitlab Pages, and the Hugo static site generator) is the
README of this project: https://gitlab.com/pages/hugo. In the meantim, this
section has a snapshot--therefore likely stale and less trustworthy--of the
relevant bits.

### tl;dr quickstart

Ignore reading the sections below and just run: `make`.

<!-- TODO setup version tagging and then update this section if needed -->

### GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in the [`.gitlab-ci.yml`](.gitlab-ci.yml) file in this folder.

### Building locally: Hugo Static Site Generator

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hugo
1. Preview your project: `hugo server`
1. Add content
1. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation][].

### Vendoring: External Source Devependencies

This repository tries to only pull in external dependencies as source code if it
can be tracked by git and easily updated (ie: automatically). `vendoring.sh`
script helps with that latter part.

The `vendoring.sh` script keeps vendored source code up to date by simply going
through its internal catalog of vendored sources this repostory cares about, and
then pulling the latest source code from the upstream repository that each came
from. Simply run `./vendoring.sh` with no arguments to do this. See "Adding
New..." below for more on this internal catalog.

#### Updating Vendored Sources

Run `./vendoring.sh` with no arguments.

Under the hood, `vendoring.sh` utilizes `git subtree` to refresh the vendoring
of each item it cares about. It does this by automatically running a command
that looks something like:

```sh
# DO NOT RUN THIS - vendoring.sh will do this automatically
git subtree pull \
  --squash \
  --message ... \
  --prefix themes/SomeExternallyAuthoredAwesomeTheme \
  https://gitserver/repo/AwesomeTheme \
  main
```

#### Adding New Sources (Vendoring)

Adding a new codebase with (we'll continue with our ficticious example values
from "Updating Vendored Sources" above) involves just two steps:

1. run `git subtree` similar to what `vendoring.sh` will run but with `add` instead of `pull`. Concretely,
   given our ficticious example, that would be a commandline that looks like:

   ```sh
    git subtree add \
      --squash \
      --message "my git commit msg explaining what this vendored source is and what its used for in this repo" \
      --prefix themes/SomeExternallyAuthoredAwesomeTheme \
      https://gitserver/repo/AwesomeTheme \
      main
   ```
2. updating the `/vendoring.sh` script in this repo so its `VENDORED_TREES` array
   has the same prefix (installation dir, relative to root of this git repo),
   repository (URL), and git-ref (eg: branch name) you chose in step (1). Concretely,
   given our ficticious example, that would be a new line in the array that
   looks like:

   ```
   themes/SomeExternallyAuthoredAwesomeTheme https://gitserver/repo/AwesomeTheme main
   ```

#### Demo: Preview a demo of this site

Clone or download this project to your local computer and run `hugo server` in
your commandline, and then a demo can be seen at `http://localhost:1313/`.

### Future Development

This section has some notes (perhaps more stale than said project) for future
reference.

#### Themeing: Look and Feel

The theme (look and feel) of the hippoeatsplants originally used was
http://themes.gohugo.io/beautifulhugo/ but that page also lists alternatives to
consider. You can also do some custom theming.

#### Custom Domains

If you'd like to setup a custom domain for hippoeatsplants, then you need to
modify your `config.toml` according to [custom domain
instructions][customDomain] so that the `baseurl` key has correct settings, eg:
`baseurl = "http(s)://example.com"`.

## Troubleshooting

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[customDomain]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains
