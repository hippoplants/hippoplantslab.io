#!/bin/bash
#
# Updates vendored subtrees
declare -r VENDORED_TREES='
# hugo theme
themes/beautifulhugo  https://github.com/halogenica/beautifulhugo  master

# Lanyon theme
themes/Lanyon https://github.com/tummychow/lanyon-hugo master

'
# DO NOT EDIT BELOW THIS LINE #################################################
###############################################################################

#TODO consider moving the above list into vendor.txt instead; if so then take it
# as an optioanl argument (and capture it _before_ the next `cd $script_dir`
# lines, of this script, take place).

# We want to have a safe baseline below for all our command; so move ourselves
# to the root of the git repo where we happen to know that this script lives.
script_dir="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)"
cd "$script_dir"

function run_git_subtree() {
  local prefix="$1" git_remote_url="$2" git_ref="$3"

  local git_subtree_cmd=pull
  local git_subtree_message
  printf -v git_subtree_message 'subtree vendoring: updates %s

utilizes `git subtree` to refresh the vendoring of "%s"; this (this
commit) is generated via the below commandline:
    ```sh
    git subtree %s \
      --squash \
      --message ... \
      --prefix %s \
      %s \
      %s
    ```

Adding a new codebase with similar vendoring capability is documented in
README.md at the root of this repository.
' "$prefix" "$prefix" "$git_subtree_cmd" "$prefix" "$git_remote_url" "$git_ref"

  git subtree "$git_subtree_cmd" \
    --squash \
    --message "$git_subtree_message" \
    --prefix "$prefix" \
    "$git_remote_url" \
    "$git_ref"
}

function print_vendor_config() {
  echo "$VENDORED_TREES" |
    sed --regexp-extended --expression '/^[[:space:]]*$/d' |
    sed --regexp-extended --expression '/^([[:space:]]*)?#/d'
}

function subtree_exists() (
  cd "$script_dir"
  [[ -e "$1" && -d "$1" ]]
)

function all_nonempty_values() {
  # Copy/pasted straight from yabashlib; specifically:
  #  https://gitlab.com/jzacsh/yabashlib/-/blob/dca53ee6a8a6c8265ed727b4c681d312e6cb0c69/src/string.sh#L41-45
  # Whether string "$1" is either empty or practically empty (whitespace).
  function strIsEmptyish() {
    local empty_regexp='^[[:space:]]*$'
    [[ -z "$1" || "$1" =~ $empty_regexp ]]
  }
  for arg in "$@"; do
    if strIsEmptyish "$@"; then
      return 1
    fi
  done
  return 0
}

function is_git_repo_clean() (
  # Copy/pasted straight from yabashlib; specifically:
  #  https://gitlab.com/jzacsh/yabashlib/-/blob/dfce3c0602aea40bf319514995db20db2d83743e/src/vcs.sh#L6-12
  function vcs_git_is_clean() (
    [[ "$#" -eq 0 ]] || {
      cd "$1" || logfFatal \
        'cannot inspect git status; failed to CD to\n\t"%s"\n' "$1"
      }
    [[ -z "$(git status --porcelain)" ]];
  )

  vcs_git_is_clean "$script_dir"
)

is_git_repo_clean || {
  printf 'error: vendoring cannot continue with a dirty git repo\n' >&2
  exit 1
}

index=-1
while read installed_dir git_remote_url git_ref;do
  index=$(( index + 1 ))
  all_nonempty_values "$installed_dir" "$git_remote_url" "$git_ref" ||  {
    printf \
      'ERROR: skipping vendored entry #%s because values incomplete[d="%s"; url="%s"; ref="%s"]\n' \
      "$index" "$installed_dir" "$git_remote_url" "$git_ref" >&2
    continue
  }
  subtree_exists "$installed_dir" || {
    printf \
      'ERROR: skipping vendored entry #%s (dir "%s") because subtree not found\n' \
      "$index" "$installed_dir" >&2
    continue
  }

  run_git_subtree "$installed_dir" "$git_remote_url" "$git_ref" || printf \
    'FATAL: vendored entry #%s (dir "%s") failed to update; continuing to next one...\n' \
    "$index" "$installed_dir" >&2
done < <(print_vendor_config)
